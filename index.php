<!doctype html>
<html lang="en">
<head>

    <title>First HomeWork</title>
    <?php
    $name = 'Denys';
    $age = 32;
    echo '<br>';
    echo 'Меня зовут ' . $name . ' мой возраст ' . $age . ' года.';
    echo '<br>';
    ?>

    <?php
    $pi = 3.14;
    print '<br>';
    print 'Теперь вывожу на экран число Пи ' . $pi;
    ?>

    <?php
    echo '<pre>';
    echo '<br>';
    $arr = ['alex', 'vova', 'tolya'];
    print_r($arr);
    echo '</pre>;'
    ?>

    <?php
    echo '<br>';
    echo '<pre>';
    $arr2 = [
        ['alex', 'vova', 'tolya'],
        ['kostya', 'olya']
    ];
     print_r($arr2);
    echo '</pre>';
    ?>

    <?php
    $arr3 = [
        'alex',
        'vova',
        'tolya',
        [
            'kostya', 'olya',
            [
                'gosha', 'mila'
            ]
        ]
    ];
    echo '<pre>';
    print_r($arr2);
    echo '</pre>';
    ?>
    <?php
    $arr4 = ['alex', 'vova', 'tolya',
        [
                'kostya', 'olya',
            ['gosha', 'mila']
        ]
            ];
    echo '<pre>';
    echo print_r($arr4);
    echo '</pre>';
    ?>
